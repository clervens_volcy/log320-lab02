import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;

public class lab02 {
	final static short NONE = 0;
	final static short OCCUPIED = 1;
	final static short FREE = 2;
	
	private static int[][] board;
	private static HashSet<String> seenBoards = new HashSet<String>();
	private static int nbPions = 0;
	private static int nbNode = 0;
	private static String[] icons = {"#", "•", " "};
	private static Stack<int[]> moves = new Stack<int[]>();
	private static Direction lastDir;
	
	public static void main(String[] args) throws FileNotFoundException {
		// WIN : Window -> Preferences -> General -> Workspace : Text file encoding : DEFAULT UTF-8
		// OSX : Eclipse -> Preferences -> General -> Workspace : Text file encoding : DEFAULT UTF-8
		
		System.out.println("Lab02 Équipe 02");
		System.out.println();
		
		Scanner sc = new Scanner(System.in);
		long time, duration;
		boolean result;
		while(true) {
			try {
				System.out.print("Enter puzzle file path (or `exit` to quit): ");
				String path = sc.nextLine();
				if (path.equalsIgnoreCase("exit") || path.equalsIgnoreCase("quit")) {
					System.out.println("...");
					sc.close();
					System.exit(0);
				}
				board = getFile(path);
				time =  System.nanoTime();
				result = solveBacktracking();
				duration = System.nanoTime() - time;
				printMoves();
				System.out.println("Solution? " + result );
				System.out.println("Duration: " + duration/ 1.0e9 + "s.");
				printBoard();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
	private static void printBoard() {
		System.out.println();
		for (int[] row : board) {
			for (int column : row) { 
				System.out.print("["+icons[column]+"]");
			}
			System.out.println();
		}
		System.out.println(nbPions + " pawns left.");
		System.out.println();
	}
	
	private static void printMoves() {
		if (moves.size() == 0) {
			System.out.println(" --- No moves --- ");
			System.out.println("Nb nodes : " + nbNode);
			return;
		}
		System.out.println("Moves list : [startX, startY, endX, endY]");
		System.out.println();
		for(int[] move : moves) {
			System.out.println(Arrays.toString(move));
		}
		System.out.println("Nb nodes : " + nbNode);
	}

	private static int[][] getFile(String path) throws IOException, FileNotFoundException {
		BufferedReader reader = null;
		int[][] num = new int[7][7];
		nbPions = 0;
		nbNode = 0;
		moves.clear();
		reader = new BufferedReader(new FileReader(path));
		int j = 0;
		String line = reader.readLine();
		while (line != null) {
			char[] letters = line.trim().toCharArray();

		    for (int i = 0; i < letters.length; i++){
		    	int val = letters[i] - '0';
		        num[j][i] = val;
		        if (val == 1) {
		        	nbPions++;
		        }
		    }
			j++;
			line = reader.readLine();

		}
		reader.close();

		return num;
	}
	
	private static boolean canMove(int posX, int posY, Direction dir) {
		if (board[posY][posX] != OCCUPIED)
			return false;
		
		switch (dir) {
		case DOWN:
			return (posY+2 < board.length && board[posY+1][posX] == OCCUPIED && board[posY+2][posX] == FREE);
		case RIGHT:
			return (posX+2 < board[posY].length && board[posY][posX+1] == OCCUPIED && board[posY][posX+2] == FREE);
		case UP:
			return (posY-1 > 0 && posY-2 > 0 && board[posY-1][posX] == OCCUPIED && board[posY-2][posX] == FREE);
		case LEFT:
			return (posX-1 > 0 && posX-2 > 0 && board[posY][posX-1] == OCCUPIED && board[posY][posX-2] == FREE);
		default:
			return false;
		}
	}
	
	private static int[] scoreMove(int posX, int posY, Direction dir) {
		int[] scores = new int[4]; // left, right, up, down
		if (board[posY][posX] != OCCUPIED)
			return scores;
		
		if (lastDir != null && dir == lastDir) {
			switch (dir) {
			case LEFT:
				++scores[0];
				break;
			case RIGHT:
				++scores[1];
				break;
			case UP:
				++scores[2];
				break;
			case DOWN:
				++scores[3];
				break;
			default:
				break;
			}
		}
		
		if (posX < 3) { // Left region
			++scores[0];
		} else if (posX > 5) { // Right region
			++scores[1];
		}
		if (posY < 3) { // Upper region
			++scores[2];
		} else if (posY > 5) { // Down region
			++scores[3];
		}
		
		return scores;
	}
	
	private static void move(int posX, int posY, Direction dir) {
		board[posY][posX] = FREE;
		switch (dir) {
		case DOWN:
			board[posY+1][posX] = FREE;
			board[posY+2][posX] = OCCUPIED;
			moves.push(new int[]{posX, posY, posX, posY+2});
			break;
		case RIGHT:
			board[posY][posX+1] = FREE;
			board[posY][posX+2] = OCCUPIED;
			moves.push(new int[]{posX, posY, posX+2, posY});
			break;
		case UP:
			board[posY-1][posX] = FREE;
			board[posY-2][posX] = OCCUPIED;
			moves.push(new int[]{posX, posY, posX, posY-2});
			break;
		case LEFT:
			board[posY][posX-1] = FREE;
			board[posY][posX-2] = OCCUPIED;
			moves.push(new int[]{posX, posY, posX-2, posY});
			break;
		}
		nbPions--;
		nbNode++;
	}
	
	private static void revertMove(int startX, int startY, int endX, int endY) {
		Direction dir = Direction.getDirectionFromMoveCoords(startX, startY, endX, endY);
		//System.out.println("Reverting { "+endX+", "+endY+" } from "+dir.name()+" direction");
		switch (dir) {
		case DOWN:
			board[startY+1][startX] = OCCUPIED;
			board[startY+2][startX] = FREE;
			break;
		case RIGHT:
			board[startY][startX+1] = OCCUPIED;
			board[startY][startX+2] = FREE;
			break;
		case UP:
			board[startY-1][startX] = OCCUPIED;
			board[startY-2][startX] = FREE;
			break;
		case LEFT:
			board[startY][startX-1] = OCCUPIED;
			board[startY][startX-2] = FREE;
			break;
		}
		board[startY][startX] = OCCUPIED;
		nbPions++;
	}
	
	private static boolean solveBacktracking() {
		if (nbPions == 1) {
			return true;
		} else if (nbNode > 20942114) {
			return false;
		} else {
			for (int i = 0; i < board.length; i++) {
				for (int j = 0; j < board[i].length; j++) {
					if (board[j][i] == OCCUPIED && !seenBoards.contains(getBoardHashValue(board))) {
						// OCCUPIED check reduce time by ~60-70%;
						// !seenBoards reduce time by ~60-85%, reduce count of checked nodes by ~99.8%
						// We're still far from 51!
						for (Direction dir : Direction.values()) {
							if (canMove(i, j, dir) ) {
								move(i, j, dir);
								if (solveBacktracking()) return true;
								seenBoards.add(getBoardHashValue(board));
								int[] move = moves.pop();
								revertMove(move[0], move[1], move[2], move[3]);
							}
						}
					}
				}
			}
			return false;
		}
	}
	
	// String to StringBuilder => Big time reduction!
	private static String getBoardHashValue(int[][] matrix) {
		StringBuilder value = new StringBuilder();
		for(int i=0;i<7;i++){
			for(int j=0;j<7;j++){
				value.append(matrix[i][j]);
			}
		}		
		return value.toString();
	}
}
