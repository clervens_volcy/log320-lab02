
public enum Direction {
	UP, RIGHT, DOWN, LEFT;
	
	public static Direction getDirectionFromMoveCoords(int startX, int startY, int endX, int endY) {
		if (startX == endX) {
			if (startY > endY) {
				return UP;
			}
			return DOWN;
		} else {
			if (startX > endX) {
				return LEFT;
			} else {
				return RIGHT;
			}
		}
	}
}
